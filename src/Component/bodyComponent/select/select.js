import { Component } from "react";

class Select extends Component {
    render () {
        return (
            <div>
                <div className="row form-group mt-2">
                    <div className="col-3">
                        <label>Country</label>
                    </div>
                    <div className="col-6">
                        <select className="form-control">
                            <option>Australia</option>
                        </select>
                    </div>
                </div>
            </div>
        )
    }
}

export default Select;