import { Component } from "react";

class Input extends Component {
    onInputChangeHandler(event) {
        console.log("Input message ...");
        console.log(event.target.value);
    }
    render () {
        return (
            <div>
                <div className="row form-group">
                    <div className="col-3">
                        <label>First Name</label>
                    </div>
                    <div className="col-6">
                        <input placeholder="Your name" className="form-control" onChange={this.onInputChangeHandler}></input>
                    </div>
                </div>
                <div className="row form-group mt-2">
                    <div className="col-3">
                        <label>Last Name</label>
                    </div>
                    <div className="col-6">
                        <input placeholder="Your Last name" className="form-control" onChange={this.onInputChangeHandler}></input>
                    </div>
                </div>
            </div>
        )
    }
}

export default Input;