import { Component } from "react";
import Btn from "./button/button";
import Input from "./input/input";
import Select from "./select/select";
import Textarea from "./textare/textare";


class BodyComponent extends Component {
    render() {
        return (
            <div className="jumbotron container pt-4" style={{ backgroundColor: "#F2F4F4" }}>
                <Input />
                <Select />
                <Textarea />
                <Btn />
            </div>
        )
    }
}

export default BodyComponent