import 'bootstrap/dist/css/bootstrap.min.css'

import BodyComponent from "./Component/bodyComponent/bodycomponent";
import TittleComponents from "./Component/tittleComponent/TittleComponents/TittleComponents";

function App() {
  return (
      <div className="container jumbotron">
      <TittleComponents/>
      <BodyComponent/>
      </div>
  );
}

export default App;
